context('Add TODO tasks', () => {
    beforeEach(() => {
      cy.visit(Cypress.env('baseURL'));
      cy.get('body').type("{esc}"); // We want to start allways in the same state.
    })
    
    it('Add pressing "+" button', () => {
        cy.clickAddButton();
        cy.addTask(Cypress.env('taskName'));
    })

    it('Add pressing "N" key', () => {
        cy.typePlusKey();
        cy.addTask(Cypress.env('taskName'));
    })

    it('Add multiple tasks with same name', () => {
        let repetitions = Cypress.env('numberOfRepetitions');
        for (var count = 0; count < repetitions; count++) {
        cy.clickAddButton();
        cy.addTask(Cypress.env('taskName'));
        }
    })
})