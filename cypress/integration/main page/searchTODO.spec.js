context('Search TODO tasks', () => {
    beforeEach(() => {
      cy.visit(Cypress.env('baseURL'));
      cy.get('body').type("{esc}");
      cy.clickAddButton();
      cy.addTask(Cypress.env('taskName')); // We want to have always a controlled data if we need to search for it
    })
    
    it('Search clicking Search button', () => {
        cy.clickSearchButton();
        cy.searchTask(Cypress.env('taskName'));
    })

    it('Search pressing Forward Slash key', () => {
        cy.typeForwardSlashKey();
        cy.searchTask(Cypress.env('taskName'));
    })

    it('Search Completed task', () => {
        cy.checkTaskAsCompleted(Cypress.env('taskName'));
        cy.clickStatus("Completed");
        cy.verifyStatus(Cypress.env('taskName'),"Completed");
    })

    it('Search Completed and Uncompleted tasks with same name', () => {
        cy.clickAddButton();
        cy.addTask(Cypress.env('taskName')); // We need to create another one in order to let one as completed and another one as uncompleted
        cy.checkTaskAsCompleted(Cypress.env('taskName'));
        cy.clickStatus("Completed");
        cy.verifyStatus(Cypress.env('taskName'),"Completed");
        cy.verifyStatus(Cypress.env('taskName'),"Active");
    })
})