//ADD TODO TASKS
Cypress.Commands.add("addTask", (taskName) => { 
    cy.get('.form-control.add-todo').type(taskName)
    .should('have.attr','value',taskName)
    .type("{enter}");
    cy.contains('.todo-item.ui-state-default.pending', taskName);
    cy.get('body').type("{esc}");
})

Cypress.Commands.add("clickAddButton",() =>{
    cy.get('.button.add').click();
})

Cypress.Commands.add("typePlusKey",() =>{
    cy.get('body').type("n");
})


//SEARCH TODO TASKS
Cypress.Commands.add("searchTask", (taskName) => { 
    cy.get('.form-control.search').type(taskName)
    cy.clickStatus('Active');
    cy.contains('.todo-item.ui-state-default.pending', taskName);
    cy.get('body').type("{esc}");
    cy.clickStatus('All');
    cy.get('body').type("{esc}");
})

Cypress.Commands.add("clickSearchButton",() =>{
    cy.get('.button.search').click();
})

Cypress.Commands.add("typeForwardSlashKey",() =>{
    cy.get('body').type("/");
})

//CHECKBOXES MANAGEMENT
Cypress.Commands.add("clickStatus",(status) =>{
    cy.get('a').contains(status)
    .click();
})

Cypress.Commands.add("checkTaskAsCompleted",(taskName) =>{
    cy.get('.checkbox')
    .contains(taskName)
    .click();
    cy.get('.todo-item.ui-state-default.completed')
    .contains(taskName);
})

Cypress.Commands.add("verifyStatus",(taskName,status) =>{
    cy.get('a').contains(status)
    .click();
    cy.contains('.todo-item.ui-state-default', taskName);
})